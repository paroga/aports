# Maintainer: Luca Weiss <luca@lucaweiss.eu>
pkgname=umockdev
pkgver=0.19.0
pkgrel=0
pkgdesc="Mock hardware devices for creating unit tests and bug reporting"
arch="all"
url="https://github.com/martinpitt/umockdev"
license="LGPL-2.1-or-later"
makedepends="eudev-dev gtk-doc meson vala libpcap-dev gobject-introspection-dev"
checkdepends="gphoto2 libgudev-dev py3-gobject3 usbutils xz"
if [ "$CARCH" != "ppc64le" ]; then
	checkdepends="$checkdepends evtest"
fi
options="!check" # fail on builders for some reason, works on CI and locally (and for upstream)
source="https://github.com/martinpitt/umockdev/releases/download/$pkgver/umockdev-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	abuild-meson \
		-Dgtk_doc=true \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
552d557aeaf68f7d685a070150bcc39ba1bc212e82e1a1ea8b39adc5ca40e57a8cb630b27caa1971ec55bc12933a3aad4047caf2f9f39651f0d4fcab814c43c1  umockdev-0.19.0.tar.xz
"
